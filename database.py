from flask import g, current_app
from pymongo import MongoClient

from logging_config import setup_logging

logger = setup_logging(__name__)
MAX_CONTACT_CODES = 6


def get_db():
    if "db" not in g:
        g.db = Database()

    return g.db


class Database:
    def __init__(self):
        self._client = MongoClient()
        self._database = self._client[current_app.config["CLOUD_APP_DB_NAME"]]
        self._contacts = self._database["contacts"]
        self._competition_codes = self._database["competition_codes"]
        self._gift_codes = self._database["gift_codes"]
        self._users = self._database["users"]

    def upsert_contacts(self, data):
        contact_codes = self.get_contact_codes(data["contact_id"])
        if contact_codes and len(contact_codes) >= MAX_CONTACT_CODES:
            return False
        else:
            self._contacts.update_one(filter={"contact_id": data["contact_id"], "status": data["status"]}, update={
                "$push": {"competition_code": data["competition_code"]}}, upsert=True)
            return True

    def get_contact_codes(self, contact_id):
        contact = self._contacts.find_one({"contact_id": contact_id, "status": "Not complete"})
        if contact is None:
            return None
        else:
            logger.debug("Found {0} Competition code for contact id={1}".format(
                len(contact.get("competition_code")), contact_id))
            contact_codes = {}
            for code in contact.get("competition_code"):
                pharmacy_code = self.find_pharmacy_codes(code)
                contact_codes[code] = pharmacy_code
            return contact_codes

    def reset_contact(self, contact_id, prize_code, prize_name):
        self._contacts.update_one(filter={"contact_id": contact_id, "status": "Not complete"},
                                  update={"$set": {"status": "Complete", "prize_code": prize_code,
                                                   "prize name": prize_name}}, upsert=False)

    def find_competition_code(self, competition_code):
        code = self._competition_codes.find_one({"competition_code": competition_code, "status": "Active"})
        if code is None:
            return None
        else:
            logger.debug("Found competition code={} with status Active".format(competition_code))
            return code

    def update_competition_code(self, competition_code):
        code = self.find_competition_code(competition_code)
        if code:
            logger.debug("Setting competition code={} status to used".format(competition_code))
            self._competition_codes.update_one(filter={"competition_code": competition_code},
                                               update={"$set": {"competition_code": competition_code,
                                                                "status": "Used"}}, upsert=False)
            return code.get("pharmacy")
        else:
            return False

    def get_gift_codes_by_name(self, data):
        contact_codes = self.get_contact_codes(data["contact_id"])
        if contact_codes and len(contact_codes) == MAX_CONTACT_CODES:
            logger.debug("Found 6 codes for contact id={0}".format(data["contact_id"]))
            gift_code = self._gift_codes.find_one({"gift_code_name": data["gift_code_name"], "status": "Active"})
            if gift_code is None and data["gift_code_name"] != "other":
                return False
            elif data["gift_code_name"] == "other":
                logger.debug("Setting contactid={0} competition status to complete".format(data["contact_id"]))
                self.reset_contact(contact_id=data["contact_id"], prize_code="Other Gift(physical item)",
                                   prize_name=data["gift_code_name"])
                return True
            else:
                logger.debug("Setting prize type {0} with code={1} used".format(data["gift_code_name"],
                                                                                gift_code.get("gift_code")))
                self._gift_codes.update_one(filter={"gift_code": gift_code.get("gift_code")},
                                            update={"$set": {"status": "Used"}})
                logger.debug("Setting contactid={0} competition status to complete".format(data["contact_id"]))
                self.reset_contact(contact_id=data["contact_id"], prize_code=gift_code.get("gift_code"),
                                   prize_name=data["gift_code_name"])
                return gift_code.get("gift_code")
        else:
            return None

    def insert_competition_code(self, data):
        self._competition_codes.insert_many(data)

    def insert_gift_code(self, data):
        self._gift_codes.update({"gift_code": data["gift_code"]}, {"$set": data}, upsert=True)

    def find_available_gift_codes(self):
        available_codes = self._gift_codes.find({"status": "Active"})
        if available_codes:
            return available_codes
        else:
            return None

    def get_competition_report(self):
        used_competition = self._competition_codes.find({"status": "Used"})
        competition_report = {"Pharmacy": [],
                              "Used Codes Amount": []}
        logger.debug("Generating competition report for {}".format(used_competition.count()))
        for code in used_competition:
            if code["pharmacy"] != "TEST" and code["pharmacy"] not in competition_report["Pharmacy"]:
                used_code_per_pharmacy = self._competition_codes.count({"pharmacy": code["pharmacy"], "status": "Used"})
                competition_report["Pharmacy"].append(code["pharmacy"])
                competition_report["Used Codes Amount"].append(used_code_per_pharmacy)
        return competition_report

    def get_users(self, username, password):
        user = self._users.find_one(filter={"username": username, "password": password})
        if user:
            return {user.get("username"): user.get("password")}
        else:
            return None

    def find_pharmacy_codes(self, competition_code):
        pharmacy_code = self._competition_codes.find_one({"competition_code": competition_code})
        if pharmacy_code is None:
            return False
        else:
            return pharmacy_code.get("pharmacy")

