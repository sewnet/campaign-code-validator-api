from database import get_db
from logging_config import setup_logging

logger = setup_logging(__name__)


def upload_gift_codes(data):
    db = get_db()

    gift_codes = {}
    logger.info("Uploading {} gift codes".format(len(data)))
    for code in data:
        gift_codes["gift_code_name"] = code["gift_name"]
        gift_codes["gift_code"] = code["code"]
        gift_codes["status"] = "Active"
        db.insert_gift_code(gift_codes)
    logger.debug("Prize codes upload complete")
    return 'Ok', 200


def upload_competition_codes(data):
    db = get_db()
    logger.debug("Uploading {} competition codes".format(len(data)))
    db.insert_competition_code(data)
    logger.debug("competition codes upload complete")
    return 'Ok', 200
