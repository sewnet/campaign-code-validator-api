from flask import Flask, request, jsonify
from flask_cors import CORS
from flask_httpauth import HTTPBasicAuth

from code_factory import upload_gift_codes, upload_competition_codes
from database import get_db
from logging_config import setup_logging

logger = setup_logging(__name__)
app = Flask(__name__)
app.config.from_object("settings")
app.logger.debug("Configured Flask app.")
cors = CORS(app, resources={r"/*": {"origins": "https://orionkeraily.fi"}})
auth = HTTPBasicAuth()


@app.route('/status', methods=["GET"])
def app_status():
    return 'Ok', 200


@app.route("/customer", methods=["POST"])
def get_user():
    db = get_db()
    contact_id = request.form["elqCid"]
    response = db.get_contact_codes(contact_id)
    if response:
        return jsonify(response), 200
    else:
        logger.info("No code for contact id {}".format(contact_id))
        return jsonify({}), 200


@app.route("/competition/codes", methods=["POST"])
def check_competition_code():
    db = get_db()
    competition_code = request.form["cusCode"].upper().strip()
    contact_id = request.form["elqCid"]
    data = {
        "competition_code": competition_code,
        "contact_id": contact_id,
        "status": "Not complete"
    }
    response = db.find_competition_code(competition_code=competition_code)
    if response:
        logger.debug(
            "added competition code {0} to contact {1}".format(competition_code, contact_id))
        update_contact = db.upsert_contacts(data=data)
        if update_contact:
            pharmacy_code = db.update_competition_code(competition_code=competition_code)
            return jsonify(pharmacy_code), 200
        else:
            return jsonify({"Error": "Max code found"}), 400
    else:
        logger.error("Invalid competition code {0} by user {1}".format(competition_code, contact_id))
        return jsonify({"Error": "Something went wrong please check your code and try again"}), 404


@app.route("/prize/codes", methods=["POST"])
def get_gift_code():
    db = get_db()
    data = {
        "contact_id": request.form["elqCid"],
        "gift_code_name": request.form["prizeType"]
    }
    response = db.get_gift_codes_by_name(data=data)
    if response:
        return jsonify(response), 200
    else:
        return jsonify({"Error": "Code not found"}), 404


@app.route("/add/competition/code", methods=["POST"])
@auth.login_required()
def add_competition_code():
    data = request.get_json(force=True)
    upload_competition_codes(data=data)
    return "Ok", 200


@app.route("/add/prize/code", methods=["POST"])
@auth.login_required()
def add_gift_code():
    data = request.get_json(force=True)
    upload_gift_codes(data=data)
    return "Ok", 200


@auth.verify_password
def verify(username, password):
    db = get_db()
    user = db.get_users(username, password)
    if user and (username and password):
        return user.get(username) == password
    else:
        return False


if __name__ == '__main__':
    app.run()
